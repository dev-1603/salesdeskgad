
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('components/login.vue') },
      { path: '/Dashboard', name:'Dashboard', component: () => import('pages/Dashboard.vue') },
      { path: '/createproject',name:'ProjectDetail', component: () => import('components/createproject.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
